
let http = require("http");

let PORT = 3000;

http.createServer((req, res) => {

	if(req.url == "/login"){
		res.writeHead( 200,
		{"Content-Type": "text/html"}
		);
		res.write("Welcome to Login Page!");
		res.end();

	}else if (req.url == "/anotherpage"){
		res.writeHead( 404,
		{"Content-Type": "text/html"}
		);
		res.write("I'm sorry the page you are looking for cannot be found");
		res.end();

	}else if (req.url == "/lastpage"){
		res.writeHead( 404,
		{"Content-Type": "text/html"}
		);
		res.write("I'm sorry the page you are looking for cannot be found");
		res.end();

	}else {

		res.writeHead(404,
			{"Content-Type":"text/plain"});
		res.end("I'm sorry the page you are looking for cannot be found");
	}

}).listen(PORT);

console.log("The terminal is successfully running.");