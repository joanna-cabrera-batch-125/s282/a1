//1.What is the directive used by Node.js in loading the module it needs?

Answer : require

//2. What node.js module contains a method for server creation?

Answer http module

//3. What is the method of the http object responsible for creating a server using Node.js?

Answer createServer();

//4. What method of the response object allows us to set status codes and content types?
 
Answer res.writeHead();

//5. Where will console.log() output its contents when run in Node.js?

Answer gitbash terminal

//6. What property of the request object contains the address's endpoint?

Answer res.end();
